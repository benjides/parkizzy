@extends('layouts.master')
@section('title', 'Búsqueda')
@section('css')
<link rel="stylesheet" href="{{asset('css/spinner.css')}}">
<link rel="stylesheet" href="{{asset('css/bootstrap-datepicker.min.css')}}">
@endsection
@section('content')
<div class="container">

    <div class="flexbox">
        <div class="search-wraper" id="search-app">
            <h3>Buscar aparcamiento</h3>
            <div class="input places">
                <label>Elige un lugar</label>
                <input type="text" id="search-box" name="places" placeholder="¿Dónde?">
            </div>
            <div class="input size">
                <label>Tamaño del vehículo</label>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="radio-inline" for="small">
                          <input name="size" id="small" value="1" type="radio">
                          Pequeño
                        </label>
                        <label class="radio-inline" for="medium">
                          <input name="size" id="medium" value="2" checked="checked" type="radio">
                          Medio
                        </label>
                        <label class="radio-inline" for="big">
                          <input name="size" id="big" value="3" type="radio">
                          Grande
                        </label>
                    </div>
                </div>
            </div>
            <div class="input date-select">
                <label>Elige un dia</label>
                <input type="text" placeholder="Fecha" class="datepckr" v-model="date">
                @{{date}}
            </div>
            <div class="input hour-select">
              <label>Elige un rango de horario</label>
              <div class="flexbox flex-align-center">
                <div class="flex-grow">
                  <input type="text" name="start_time" placeholder="Hora de inicio" v-model="start_time" >
                </div>
                <span class="arrow">&rarr;</span>
                <div class="flex-grow">
                  <input type="text" name="end_time" placeholder="Hora final" v-model="end_time" >
                </div>
              </div>
            </div>

            <div class="input booking flexbox">
                <input type="submit" value="Reservar" class="flex-grow" {{ !Auth::user() ? "disabled" : "" }}>
            </div>
            @if (!Auth::user())
              <div class="disclaimer not-log">
                <p>No podrás hacer reservas si no estás registrado.</p>
                <p>Regístrate <a href="{{url('/register')}}">ahora</a></p>
              </div>
            @endif

        </div>
        <div class="map-wraper flex-grow">
            <div class="loading-cover flexbox flex-grow flex-align-center flex-justify-center">
                <div class="loader">
                    <svg class="circular">
                      <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="4" stroke-miterlimit="10"/>
                    </svg>
                </div>
            </div>
            <div id="map"></div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('js/map.js')}}" charset="utf-8"></script>
<script src="{{asset('js/bootstrap-datepicker.min.js')}}" charset="utf-8"></script>
<script src="{{asset('js/bootstrap-datepicker.es.min.js')}}" charset="utf-8"></script>
<script src="{{asset('js/moment.js')}}" charset="utf-8"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://unpkg.com/vue"></script>
<script type="text/javascript">

    var markers = {!! $markers !!};

    var app = new Vue({
      el: '#search-app',
      data: {
        date: moment(),
        start_time: '',
        end_time: ''
      }
    });

    $('.datepckr').datepicker({
        language: "es",
        autoclose: true,
        todayHighlight: true
    });
    function init() {
        toggleShow();
        var map = initMap('map');
        initSearchBox('search-box', map);
        loadMarkers(map,markers,function(marker,parking){
          console.log(parking);
        })
    }
    function toggleShow() {
        $('.loading-cover').toggleClass('loading-cover-toggle');
    }

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZPQbRiLVI-wwu5xyRu0926lxx_1qZyTw&libraries=place&callback=init&libraries=places" async defer></script>
@endsection
