@extends('layouts.master')
@section('title', 'Reservas')
@section('content')
<div class="container flexbox settings-wraper">
    <div class="settings-menu hidden-xs">
        <ul class="user-options" role="tablist">
            <li class="active" role="presentation"><a href="#active" role="tab" data-toggle="tab">Reservas activas</a></li>
            <li role="presentation"><a href="#old" role="tab" data-toggle="tab">Reservas pasadas</a></li>
        </ul>
    </div>
    <div class="tab-content settings-items">
        <div role="tabpanel" class="tab-pane fade-in active" id="active">

        </div>
        <div role="tabpanel" class="tab-pane fade" id="old">

        </div>
    </div>
</div>
@endsection
