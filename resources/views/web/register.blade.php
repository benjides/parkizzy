@extends('layouts.master')

@section('title', 'Registro')

@section('content')
<div class="container">
    <h1 class="hero">Registrate en <span class="main-color">Parkizzy</span></h1>
    <div class="form-wraper">

        <form action="{{url('/register')}}" method="POST" class="register-form">
            {{ csrf_field() }}

            <div class="input flexbox">
              <div class="input-wraper">
                <label for="firstName">Nombre</label>
                <input type="text" name="firstName" placeholder="Nombre" required="" value="{{old('firstNames')}}">
              </div>
              <div class="input-wraper">
                <label for="lastName">Apellidos</label>
                <input type="text" name="lastName" placeholder="Apellidos" required="" value="{{old('lastName')}}">
              </div>
            </div>


            <div class="input">
                <label for="email">Email</label>
                <input type="email" name="email" placeholder="test@ejemplo.com" required="" value="{{old('email')}}">
            </div>

            <div class="input flexbox">
              <div class="input-wraper flex-grow">
                  <label for="password">Contraseña</label>
                  <input type="password" name="password" placeholder="*******" required="">
              </div>
              <div class="input-wraper">
                  <label for="password_confirmation">Confimar contraseña</label>
                  <input type="password" name="password_confirmation" placeholder="*******" required="">
              </div>
            </div>

            <label for="date">Fecha de nacimiento</label>
            <div class="input flexbox flex-no-wrap">
              <select class="flex-grow" name="day">
                @for ($i=1; $i <= 31 ; $i++)
                  <option value="{{$i}}">{{$i}}</option>
                @endfor
              </select>
              <select class="flex-grow" name="month">
                @for ($i=1; $i <= 12 ; $i++)
                  <option value="{{$i}}">{{$i}}</option>
                @endfor
              </select>
              <select class="flex-grow" name="year">
                @for ($i=1980; $i <= date('Y') ; $i++)
                  <option value="{{$i}}">{{$i}}</option>
                @endfor
              </select>
            </div>

            <div class="input">
                <label for="phone">Telefono móvil</label>
                <input type="text" name="phone" placeholder="Telefono" value="{{old('phone')}}">
            </div>


            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="input submit flexbox flex-justify-center">
                <input type="submit" value="Registrate">
            </div>
        </form>
    </div>


</div>

@endsection
