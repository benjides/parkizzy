@extends('layouts.master')

@section('title', 'Registro')

@section('content')
<div class="container">
    <h1 class="hero">Gracias por registrarse en  <span class="main-color">Parkizzy</span>
      <p>Se le ha enviado un mail de confirmacion al correo electronico proporcionado.</p>
    </h1>
</div>

@endsection
