@extends('layouts.master')
@section('title', 'Inicio de sesión')
@section('content')
<div class="container">
    <h1 class="hero">
      Iniciar sesión en <span class="main-color">Parkizzy</span>
    </h1>
    <div class="form-wraper">

        <form action="{{url('/login')}}" method="POST" class="login-form">

            {{ csrf_field() }}
            @if (session()->has('message'))
            <div class="flexbox flex-justify-center">
                <span class="error-color">{{session()->get('message')}}</span>
            </div>
            @endif
            <div class="input">
                <label for="mail">Email</label>
                <input type="mail" name="mail" placeholder="test@ejemplo.com">
            </div>
            <div class="input">
                <label for="password">Contraseña</label>
                <input type="password" name="password" placeholder="*******">
            </div>
            <div class="flexbox flex-align-center">

                <div class="checkbox flex-grow">
                    <label><input type="checkbox" name="remember" value="true">Recuerdame</label>
                </div>
                <a href="{{url('/password')}}">Recuperar contraseña</a>
            </div>
            <div class="input submit flexbox flex-justify-center">
                <input type="submit" value="Iniciar sesión">
            </div>
        </form>
        <hr>
        <div class="disclaimer flexbox">
            <span class="flex-grow">¿No tienes cuenta?</span>
            <a href="{{url('/register')}}">Registrarse</a>
        </div>
    </div>

</div>
@endsection
