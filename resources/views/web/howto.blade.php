@extends('layouts.master') 
@section('title', 'Como funciona')
@section('content')
<div class="container">
    <h1 class="hero">Como usar <span class="main-color">Parkizzy</span></h1>
    <div class="steps-wraper">
        <div class="step flexbox flex-align-center">
            <img src="{{asset('/img/howto/1.png')}}" class="step-img">
            <div class="step-info flex-grow">
                <h3>Paso 1</h3>
                <p>Registrate en <a href="{{url('/login')}}">parkizzy.com</a> y consigue tu mando Parkizzy para poder disfrutar de todas las ventajas.</p>
                <p>Este mando te permitira acceder a los aparcamientos que reserves con total fiabilidad y seguridad.</p>
            </div>
        </div>
        <div class="step flexbox flex-align-center">
            <img src="{{asset('/img/howto/2.png')}}" class="step-img">
            <div class="step-info flex-grow">
                <h3>Paso 2</h3>
                <p>Haz una reserva de una plaza de aparcamiento desde la App de Parkizzy o desde la <a href="{{url('/search')}}">web</a> para la fecha elegida</p>
            </div>
        </div>
        <div class="step flexbox flex-align-center">
            <img src="{{asset('/img/howto/3b.png')}}" class="step-img">
            <div class="step-info flex-grow">
                <h3>Paso 3</h3>
                <p>Dirigite con tu vehiculo a la plaza seleccionada y a la hora de tu reserva y sigue las instruccion dentro de la aplicación para abrir la puerta con tu mando Parkizzy.</p>
            </div>
        </div>
        <div class="step flexbox flex-align-center">
            <img src="{{asset('/img/howto/4.png')}}" class="step-img">
            <div class="step-info flex-grow">
                <h3>Paso 4</h3>
                <p>¡Disfruta de tu plaza de aparcamiento!</p>
            </div>
        </div>
    </div>
</div>
@endsection
