@extends('layouts.master')

@section('title', 'Aparcamiento colaborativo')

@section('content')
<div class="container">
    <h1 class="hero">
      <span class="main-color">Encuentra aparcamiento ahora.</span>
      Comparte y encuentra plazas de garaje en tu ciudad.
    </h1>
    <form action="{{url('/search')}}" class="search-form" method="post">
      <span>Encuentra aparcamiento para hoy.</span>
        <div class="search-bar flexbox">
            <div class="input places">
                <label>Elige un lugar</label>
                <input type="text" id="search-box" name="places" placeholder="¿Dónde?">
            </div>
            <div class="input hour">
                <label>Elige un rango de horario</label>
                <div class="flexbox flex-align-center">
                  <div class="flex-grow">
                    <input type="text" name="start_time" placeholder="Hora de inicio">
                  </div>
                  <span class="arrow">&rarr;</span>
                  <div class="flex-grow">
                    <input type="text" name="end_time" placeholder="Hora final">
                  </div>
                </div>
            </div>

        </div>
        <div class="flexbox flex-grow flex-justify-end">
            <input type="submit" value="Buscar">
        </div>
    </form>


    <h2>Échale un vistazo a los aparcamientos en este momento</h2>
</div>

<div id="map"></div>
@endsection

@section('js')
<script src="{{ asset('js/map.js')}}" charset="utf-8"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZPQbRiLVI-wwu5xyRu0926lxx_1qZyTw&libraries=place&callback=init&libraries=places" async defer></script>
<script type="text/javascript">
  var markers = {!! $markers !!};
  function init() {
    var map = initMap('map');
    initSearchBox('search-box');
    loadMarkers(map,markers);
  }
</script>

@endsection
