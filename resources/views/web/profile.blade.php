@extends('layouts.master')
@section('title', 'Perfil')
@section('content')
<div class="container flexbox settings-wraper">
    <div class="settings-menu hidden-xs">
        <ul class="user-options" role="tablist">
            <li class="active" role="presentation"><a href="#personal-info" role="tab" data-toggle="tab">Información personal</a></li>
            <li role="presentation"><a href="#remote" role="tab" data-toggle="tab">Mi Mando</a></li>
            <li role="presentation"><a href="#parkings" role="tab" data-toggle="tab">Plazas</a></li>
            <li role="presentation"><a href="#homes" role="tab" data-toggle="tab">Casas</a></li>
            <li role="presentation"><a href="#cars" role="tab" data-toggle="tab">Vehiculos</a></li>
            <li role="presentation"><a href="#billing" role="tab" data-toggle="tab">Direccion de facturacion</a></li>
        </ul>
    </div>
    <div class="tab-content settings-items">
        <div role="tabpanel" class="tab-pane fade-in active" id="personal-info">

        </div>
        <div role="tabpanel" class="tab-pane fade" id="remote">

        </div>
        <div role="tabpanel" class="tab-pane fade" id="parkings">

        </div>
        <div role="tabpanel" class="tab-pane fade" id="homes">

        </div>
        <div role="tabpanel" class="tab-pane fade" id="cars">

        </div>
        <div role="tabpanel" class="tab-pane fade" id="billing">

        </div>
    </div>
</div>
@endsection
