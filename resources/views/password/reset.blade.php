@extends('layouts.master')
@section('title', 'Recuperar contraseña')

@section('content')
<div class="container">
    <h1 class="hero">
      Recuperar contraseña de <span class="main-color">Parkizzy</span>
    </h1>
    <div class="form-wraper">

        <form action="{{url('/password/reset')}}" method="POST">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="token" value="{{ $token }}">


            <div class="input">
                <label for="mail">Email</label>
                <input type="mail" name="email" placeholder="test@ejemplo.com" value="{{ old('email') }}">
            </div>

            <div class="input">
                <label for="password">Contraseña</label>
                <input type="password" name="password" placeholder="*******">
            </div>

            <div class="input">
                <label for="password">Confirmar contraseña</label>
                <input type="password" name="password_confirmation" placeholder="*******">
            </div>

            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <p>Se han producido algunos errores.</p>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="input submit flexbox flex-justify-center">
                <input type="submit" value="Enviar">
            </div>
        </form>
    </div>

</div>
@endsection
