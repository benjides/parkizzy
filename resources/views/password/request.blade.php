@extends('layouts.master')
@section('title', 'Recuperar contraseña')

@section('content')
<div class="container">
    <h1 class="hero">
      Recuperar contraseña
    </h1>
    <div class="form-wraper">

        <form action="{{url('/password')}}" method="POST">
            {{ csrf_field() }}
            <div class="input">
                <label for="mail">Email</label>
                <input type="mail" name="email" value="{{ old('email') }}" placeholder="test@ejemplo.com">
            </div>

            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <p>Se han producido algunos errores.</p>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="input submit flexbox flex-justify-center">
                <input type="submit" value="Enviar Mail">
            </div>
        </form>
    </div>

</div>
@endsection
