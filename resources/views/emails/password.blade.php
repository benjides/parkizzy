@extends('layouts.emails')


@section('content')
  <h1>Restaurar contraseña</h1>
  <p>Haga click en el siguiente enlace para poder restaurar su contraseña.</p>
  <p>Una vez modificada la contraseña antigua no será válida y dejara de funcionar.</p>
  <table>
    <tr>
      <td align="center">
        <a class="btn" href='{{ url('password/reset', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}'>Resetear</a>
      </td>
    </tr>
  </table>
@endsection
