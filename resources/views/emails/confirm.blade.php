@extends('layouts.emails')


@section('content')

  <h1>¡Gracias por su registro!</h1>
  <p>
    Esta a un solo paso para poder disfrutar de todas las ventajas de ser usuario de <a href="{{url('/')}}">parkizzy.com</a>
  </p>
  <p>
    Haga click en el siguiente enlace para confirmar su cuenta.
  </p>
  <table>
    <tr>
      <td align="center">
        <a class="btn" href='{{ url("register/confirm/{$user->token}") }}'>Confirmar</a>
      </td>
    </tr>
  </table>

@endsection
