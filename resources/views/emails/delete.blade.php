@extends('layouts.emails')


@section('content')
  <h1>Eliminacion de Usuario</h1>
  <p>
    Haga click en el siguiente enlace para proceder a eliminar permanentemente su cuenta.
  </p>
  <p>
    Tenga en cuenta que esta acción es irreversible.
  </p>
  <table>
    <tr>
      <td align="center">
        <a class="btn" href='{{ url("delete/confirm/{$user->token}") }}'>Eliminar</a>
      </td>
    </tr>
  </table>
@endsection
