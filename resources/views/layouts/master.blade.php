<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="google-site-verification" content="khjeyJoFlBE0cCKRZ00FZVFEAE2vvHNwA7fRu6NcxKY" />

    <title>
        Parkizzy | @yield('title')
    </title>


    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    @yield('css')
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body role="document">

    <header>
        <div class="container flexbox flex-align-center">
            <a href="{{url('/')}}">
                <img src="{{asset('img/logo.png')}}" alt="Logo" class="logo">
            </a>
            <div class="flexbox flex-align-center flex-justify-end flex-grow">
                <ul class="inline-menu">
                    <li><a class="button" href="{{ url('/search') }}">aparcar</a></li>
                    @if (Auth::user())
                    <li class="dropdown">
                      <span class="dropdown-toggle" role="button" id="menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <span class="username">{{Auth::user()->firstName}}</span>
                        <span class="caret"></span>
                      </span>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="menu">
                          <li><a href="{{url('/bookings')}}">Mis reservas</a></li>
                          <li><a href="{{url('/profile')}}">Ajustes</a></li>
                          <li role="separator" class="divider"></li>
                          <li><a href="{{url('logout')}}">Cerrar Sesión</a></li>
                        </ul>
                    </li>
                    @else
                    <li><a class="" href="{{ url('/login') }}">Iniciar sesión</a></li>
                    <li><a class="" href="{{ url('/register') }}">Regístrate</a></li>
                    @endif
                    <li><a class="" href="{{ url('/howto') }}">Como funciona</a></li>
                </ul>
            </div>
        </div>
    </header>

    @yield('content')

    <footer>
        <div class="container">
            <div class="flexbox flexbox flex-align-center">
                <span class="disclaimer">PARKIZZY © {{date('Y')}}</span>
                <ul class="inline-menu flex-grow">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li><a href="{{url('/search')}}">Buscar</a></li>
                </ul>
                <img class="footer-logo" src="{{asset('/img/logob.png')}}" alt="logo">
            </div>
            <hr>
            <div class="flexbox">
                <ul class="inline-menu flex-grow">

                </ul>
                <ul class="inline-menu">
                    <li><a href="{{url('faqs')}}">FAQs</a></li>
                    <li><a href="{{url('about')}}">Sobre</a></li>
                    <li><a href="{{url('cookies')}}">Política de Cookies</a></li>
                    <li><a href="{{url('privacy')}}">Privacidad</a></li>
                </ul>
            </div>
        </div>
    </footer>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    @yield('js')

</body>

</html>
