<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<meta name="viewport" content="width=device-width" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

	<style media="screen">
		* {
			box-sizing: border-box;
			margin: 0;
			padding: 0;
			font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
		}

		img {
			max-width: 100%;
		}

		.collapse {
			margin: 0;
			padding: 0;
		}

		body {
			-webkit-font-smoothing: antialiased;
			-webkit-text-size-adjust: none;
			width: 100%!important;
			height: 100%;
		}

		a {
			color: #2BA6CB;
		}

		.btn {
			text-decoration: none;
			color: #FFF;
			background-color: #369742;
			border-radius: 50px;
			padding: 10px 30px;
			font-weight: bold;
			font-size: 12px;
			text-transform: capitalize;;
			text-align: center;
			cursor: pointer;
			display: inline-block;
			margin-top: 10px;
		}


		table.head-wrap {
			width: 100%;
		}

		.header {
			border-bottom: 1px solid #d7d7d7;
		}

		.header.container table td.logo img {
			padding: 20px;
			height: 80px;
		}

		table.body {
			width: 100%;
		}

		table.footer {
			width: 100%;
			clear: both!important;
		}

		table.footer .container td.disclaimer p {
			border-top: 1px solid #d7d7d7;
			padding-top: 15px;
			font-size: 10px;
			font-weight: bold;
		}

		h1 {
			font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
			line-height: 1.1;
			margin-bottom: 35px;
			color: #000;
			font-weight: 200;
			font-size: 35px;
		}


		.collapse {
			margin: 0!important;
		}

		p,
		ul {
			margin-bottom: 10px;
			font-weight: normal;
			font-size: 14px;
			line-height: 1.6;
		}

		.container {
			display: block!important;
			max-width: 600px!important;
			margin: 0 auto!important;
			clear: both!important;
		}

		.content {
			padding: 15px;
			max-width: 600px;
			margin: 0 auto;
			display: block;
		}

		.content table {
			width: 100%;
		}

		.clear {
			display: block;
			clear: both;
		}


		@media only screen and (max-width: 600px) {
			a[class="btn"] {
				display: block!important;
				margin-bottom: 10px!important;
				background-image: none!important;
				margin-right: 0!important;
			}
		}

	</style>

</head>

<body bgcolor="#FFFFFF">

	<table class="head-wrap">
		<tr>
			<td></td>
			<td class="header container">
					<table>
						<tr>
							<td class="logo">
								<img src="{{asset('img/logo.png')}}" >
							</td>
						</tr>
					</table>
			</td>
			<td></td>
		</tr>
	</table>


	<table class="body">
		<tr>
			<td></td>
			<td class="container" bgcolor="#FFFFFF">

				<div class="content">
					<table>
						<tr>
							<td>
								@yield('content')
							</td>
						</tr>
					</table>
				</div>

			</td>
			<td></td>
		</tr>
	</table>


	<table class="footer">
		<tr>
			<td></td>
			<td class="container">
				<div class="content">
					<table>
						<tr>
							<td class="disclaimer" align="center">
								<p>Parkizzy @ {{ date('Y') }}</p>
							</td>
						</tr>
						<tr>
							<td align="center">
								<p>
									<a href="#">Terminos</a> |
									<a href="#">Privacidad</a> |
									<a href="#">Contacto</a>
								</p>
							</td>
						</tr>
					</table>
				</div>

			</td>
			<td></td>
		</tr>
	</table>
</body>

</html>
