<!DOCTYPE html>
<html>
  <head>
    <title>Parkizzy Map</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }

      #map {
        height: 100%;
      }

      .controls {
        position: absolute;
        z-index:30;
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 300px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #infowindow {
        display:none;
      }
    </style>
  </head>
  <body>


    <input id="pac-input" class="controls" type="text" placeholder="Encuentra aparcamiento">
    <div id="map"></div>
    <div id="infowindow">
      <div id="content">
        <div id="siteNotice"></div>
        <h3 id="firstHeading" class="firstHeading">@{{title}}</h3>
        <div id="bodyContent">
          @{{{body}}}
        </div>
      </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.5/handlebars.js" charset="utf-8"></script>

    <script type="text/javascript">
      var markers = {!! $parkings !!};
      var map;
      var icons = ['green-dot.png','yellow-dot.png','red-dot.png'];
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          mapTypeControl: false,
          panControl: false,
          streetViewControl: false,
          zoomControlOptions: {
            position: google.maps.ControlPosition.LEFT_BOTTOM
          }
        });

        @if(count($center))
        var pos = {!! $center !!};
        map.setCenter(pos);
        @else
        navigator.geolocation.getCurrentPosition(function(position) {
          var pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };
          map.setCenter(pos);
        });
        @endif

        for (var i = 0; i < markers.length; ++i) {
          var mark = markers[i];
          var marker = new google.maps.Marker({
            position: {
              lat: mark.lat,
              lng: mark.long
            },
            icon : 'https://maps.google.com/mapfiles/ms/icons/'+icons[mark.status],
            map: map
          });
          attachInfo(marker, mark);
        }



        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);


        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();
          if (places.length == 0) {
            return;
          }
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            if (place.geometry.viewport) {
              bounds.union(place.geometry.viewport);
            }else{
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });


      }

      function attachInfo(marker,mark) {
        var infowindow = new google.maps.InfoWindow({
          content: buildcontent(mark)
        });
        marker.addListener('click', function() {
          infowindow.open(marker.get('map'), marker);
          marker.get('map').setCenter(marker.getPosition());
          window.AppInventor.setWebViewString(mark.app);
        });
      }

      function buildcontent(mark){
        var content = document.getElementById('infowindow').innerHTML;
        var template = Handlebars.compile(content);
        var context = {title: mark.address+','+mark.number , body: mark.message};
        var html = template(context);
        return html;
      }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBZPQbRiLVI-wwu5xyRu0926lxx_1qZyTw&libraries=place&callback=initMap&libraries=places" async defer></script>

    {{-- <script src="{{asset('js/map.js')}}"></script> --}}


  </body>
</html>
