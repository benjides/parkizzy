@extends('layouts.master')

@section('title', 'Registro')

@section('content')
<div class="container">
    <h1 class="hero">
      <p>Usuario verificado</p>
      Gracias por usar <span class="main-color">Parkizzy</span>
    </h1>
</div>

@endsection
