@extends('layouts.master')

@section('title', 'Registro')

@section('content')
<div class="container">
    <h1 class="hero">Su cuenta en <span class="main-color">Parkizzy</span> ha sido correctamente eliminada
      <p>Esperamos volver a verte pronto.</p>
    </h1>
</div>

@endsection
