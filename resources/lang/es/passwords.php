<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Su contraseña ha sido reseteada',
    'sent' => 'Le hemos enviado un mail con instrucciones para resetear su contraseña',
    'token' => 'El token proporcionado ha caducado. Vuelva a repetir el proceso.',
    'user' => "No se encuentra un usuario con esa direccion de correo.",

];
