var icons = ['green-dot.png', 'yellow-dot.png', 'red-dot.png'];

function initMap(id) {
    var map;
    map = new google.maps.Map(document.getElementById(id), {
        zoom: 15,
        mapTypeControl: false,
        panControl: false,
        streetViewControl: false,
        scrollwheel: false
    });
    navigator.geolocation.getCurrentPosition(function(position) {
        var center = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        map.setCenter(center);
    });
    return map;
}

function initSearchBox(id, map) {

    var input = document.getElementById(id);
    var searchBox = new google.maps.places.Autocomplete(input);

    if (map === undefined) {
        return;
    }

    searchBox.addListener('place_changed', function() {
        var place = searchBox.getPlace();
        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }

    });
}

function loadMarkers(map, markers,cllbck) {
    for (var i = 0; i < markers.length; i++) {
        var parking = markers[i];
        var marker = new google.maps.Marker({
            position: {
                lat: parking.lat,
                lng: parking.long
            },
            icon: 'https://maps.google.com/mapfiles/ms/icons/' + icons[parking.status],
            map: map
        });
        prepareMarker(marker,parking,cllbck);
        function prepareMarker(marker,parking,cllbck){
          var infowindow = new google.maps.InfoWindow({
            content: buildcontent(parking)
          });
          marker.addListener('click', function() {
            infowindow.open(marker.get('map'), marker);
            marker.get('map').setCenter(marker.getPosition());
            if (cllbck !== undefined) {
              cllbck(marker,parking);
            }
          });
        }
        function buildcontent(parking) {
          return `<div id="infowindow">
            <div id="content">
              <div id="siteNotice"></div>
              <h3 id="firstHeading" class="firstHeading">${parking.address+','+parking.number}</h3>
              <div id="bodyContent">
                ${parking.message}
              </div>
            </div>
          </div>`
        }
    }
}
