<?php

namespace App;

use App\Elegant;

class Billing extends Elegant
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'address',
      'street',
      'num_street',
      'num_block',
      'floor',
      'door',
      'zip',
      'city',
      'province',
  ];


  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
      'id', 'user_id'
  ];

  /**
   * Determines if the model should be timestamped.
   *
   * @var boolean
   */
  public $timestamps = false;


  /**
   * Get the user that owns the parking.
   */
  public function user()
  {
    return $this->belongsTo('App\User');
  }

}
