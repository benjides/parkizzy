<?php

namespace App;

use Carbon\Carbon;
use App\Utils\Interval;

class Share extends Elegant
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parking_id',
        'start_date',
        'end_date',
        'repeat',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'user_id',
    ];

    /**
     * The attributes that should be casted as dates.
     *
     * @var array
     */
    protected $dates = [
      'start_date', 'end_date',
    ];

    /**
     * Determines if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Set start_date attribute.
     *
     * @param Carbon $date
     */
    public function setStartDateAttribute($date)
    {
        $this->attributes['start_date'] = $date->second(0);
    }

    /**
     * Set start_date attribute.
     *
     * @param Carbon $date
     */
    public function setEndDateAttribute($date)
    {
        $this->attributes['end_date'] = $date->second(0);
    }

    /**
     * Get the parking that owns the Share.
     */
    public function parking()
    {
        return $this->belongsTo('App\Parking');
    }

    /**
     * Get the bookings of the Share.
     */
    public function bookings()
    {
        return $this->hasMany('App\Booking');
    }

    public function getIntervalsAttribute()
    {
        $intervals = $this->interval;
        $bookings = $this->bookings()->intervals();

        return $this->freeIntervals($intervals, $bookings);
    }

    /**
     * Return the Share as Interval Model.
     *
     * @return Intervals[]
     */
    public function getIntervalAttribute()
    {
        //By NOW
        return collect([new Interval($this->start_date, $this->end_date)]);
        $intervals = [];
        if ($this->repeat != 0) {
            $date = $this->start_date;
            while ($date < $this->end_date) {
                //Due to dayOfWeek puts Sunday at 0.
              $index = ($date->dayOfWeek + 6) % 7;
                if ($this->repeat[$index % 7] == 1) {
                    $intervals[] = new Interval($date->copy(), $date->copy(), $this->start_time, $this->end_time);
                }
                $date->addDay();
                ++$index;
            }
        } else {
            $intervals[] = new Interval($this->start_date, $this->end_date);
        }

        return collect($intervals);
    }

     /**
      * Provides the intervals at which the share is available.
      *
      * @param Collection $intervals
      * @param Collection $bookings
      *
      * @return Collection<Interval>
      */
     private function freeIntervals($intervals, $bookings)
     {
         $total = array();
         if ($bookings->count() == 0) {
             $total = $intervals;

             return collect($total);
         }

         $interval = new Interval();
         $booking = $bookings->shift();
         if ($booking->start_date > $this->start_date) {
             $interval->start_date = $this->start_date;
             $interval->end_date = $booking->start_date->copy()->subMinutes(config('app.margin'));
             $total[] = $interval;
             $interval = new Interval();
         }
         $interval->start_date = $booking->end_date->copy()->addMinutes(config('app.margin'));

         for ($i = 0; $i < $bookings->count(); ++$i) {
             $booking = $bookings[$i];
             $interval->end_date = $booking->start_date->copy()->subMinutes(config('app.margin'));
             $total[] = $interval;
             $interval = new Interval();
             $interval->start_date = $booking->end_date->copy()->addMinutes(config('app.margin'));
         }

         if ($booking->end_date < $this->end_date) {
             $interval->end_date = $this->end_date;
             $total[] = $interval;
         }

         return collect($total);
     }

    /**
     * Returns the Interval at which the share is available.
     *
     * @param Interval $request
     *
     * @return array
     */
    public function avaibility(Interval $request)
    {
      return $this->intervals
      ->reject(function ($interval) use ($request) {
      //Remove off-limits intervals and small intervals
       return $request->start_date > $interval->end_date
           || $interval->size() < config('app.min_interval');
      })
      ->map(function ($interval) use ($request){
       return [
                'interval' => $interval,
                'status' => $interval->includesPartial($request)
              ];
      })
      //Get the best fitting interval
      ->sortBy('status')
      ->values()
      ->first();
    }

    /**
     * Scope a query to only include active Shares.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return Collection<Share>
     */
    public function scopeActive($query)
    {
        return $query->where('end_date', '>', Carbon::now())
                     ->orderBy('start_date')
                     ->get();
    }

    /**
     * Scope a query to only include expired Shares.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return Collection<Share>
     */
    public function scopeExpired($query)
    {
        return $query->where('end_date', '<=', Carbon::now())
                     ->orderBy('start_date')
                     ->get();
    }

    /**
     * Get description attribute.
     */
    public function getDescriptionAttribute()
    {
        $interval = new Interval($this->start_date, $this->end_date);
        $result = '';
        switch ($this->repeat) {
          case 0:
            $result .= $interval;
            break;
          case 1111111:
            $result = 'Todos los dias, '.strtolower($interval);
            break;
          default:
            $days = [];
            $daylist = ['lunes', 'martes', 'miércoles', 'jueves', 'viernes', 'sábados', 'domingos'];
            for ($i = 0; $i < 7; ++$i) {
                if ($this->repeat[$i] == 1) {
                    $days[] = $daylist[$i];
                }
            }
            $last = array_pop($days);
            $string = count($days) ? implode(', ', $days).' y '.$last : $last;
            $result = 'Todos los '.$string.' '.strtolower($interval);
            break;
        }

        return $result;
    }

    /**
     * Initializes the model.
     */
    public static function boot()
    {
        parent::boot();

        // Avoid deleting shares which have active bookings
        static::deleting(function ($share) {
            $bookings = $share->bookings()->active()->count();

            return $bookings > 0 ? false : true;
        });
    }
}
