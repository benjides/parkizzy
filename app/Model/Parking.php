<?php

namespace App;

use File;
use App\Utils\Marker;
use App\Utils\Interval;

class Parking extends Elegant
{
    /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'address_parking',
      'street_parking',
      'num_street_parking',
      'num_parking',
      'level_parking',
      'zip_parking',
      'city_parking',
      'province_parking',
      'car_size',
      'wc',
      'cctv',
      'indoor',
      'street_level',
      'easy',
      'doorman',
      'description_parking',
      'plaza_photo',
      'plano_photo',
      'street_photo',
      'long_parking',
      'lat_parking',
      'rolling',
      'verify_opener',
      'code_1',
      'code_2',
      'code_3',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
      'id', 'user_id'
  ];

  /**
   * The attributes that should be casted.
   *
   * @var array
   */
  protected $casts = ['long_parking' => 'float', 'lat_parking' => 'float'];

  /**
   * Determines if the model should be timestamped.
   *
   * @var boolean
   */
  public $timestamps = false;

  /**
   * Get the plaza_photo attribute.
   *
   * @param string $date
   */
  public function getPlazaPhotoAttribute($value)
  {
      return $value ? $value : 'default.png';
  }

  /**
   * Get the pano_photo attribute.
   *
   * @param string $date
   */
  public function getPlanoPhotoAttribute($value)
  {
      return $value ? $value : 'default.png';
  }

  /**
   * Get the street_photo attribute.
   *
   * @param string $date
   */
  public function getStreetPhotoAttribute($value)
  {
      return $value ? $value : 'default.png';
  }

  /**
   * Get the user that owns the parking.
   */
  public function user()
  {
      return $this->belongsTo('App\User');
  }

  /**
   * Get the shares that belongs to the parking.
   */
  public function shares()
  {
      return $this->hasMany('App\Share');
  }

  /**
   * Get the code that belongs to the parking.
   */
  public function code()
  {
      return $this->hasOne('App\Code');
  }

  /**
   * Get the bookings that belongs to the parking.
   */
  public function bookings()
  {
      return $this->hasManyThrough('App\Booking', 'App\Share');
  }

  /**
   * Returns the avaiblity for each Parking.
   *
   * @param  Interval $interval
   *
   * @return Collection<Marker>
   */
  public static function available($interval)
  {
      $parkings = self::all();
      return $parkings->map(function($parking) use ($interval){
        return $parking->parkingAvaibility($interval);
      });
  }

  /**
   * Returns the avaiblity for the Parking.
   *
   * @param  Interval $interval
   *
   * @return Marker
   */
  public function parkingAvaibility($interval)
  {
      $avaibility = $this->avaibility($interval);
      if (!$avaibility || !$avaibility['interval']) {
          return Marker::unavailable($this);
      }
      return Marker::make($this,$avaibility['share'],$avaibility['interval'],$avaibility['status']);
  }

  /**
   * Returns the most suitable avaibility for the Parking.
   *
   * @param  Interval $interval
   *
   * @return array ['share' => $share , 'interval' => $interval, 'status' => $status]
   */
  public function avaibility($interval)
  {
      $shares = $this->availableShares($interval);
      //The parking has no shares
      if (count($shares) === 0) {
          return null;
      }

      //Check avaibility for each share;
      return $shares
      ->map(function($share) use ($interval){
        $avaibility = $share->avaibility($interval);
        return [
          'share' => $share,
          'interval' => $avaibility['interval'],
          'status' => $avaibility['status']
        ];
      })
      ->sortBy('status')
      ->values()
      ->first();
  }

  /**
   * Returns the Shares avilable for a given Interval.
   *
   * @param Interval $interval
   *
   * @return array
   */
  public function availableShares($interval)
  {
      return $this->shares()
                  ->where('end_date','>',$interval->start_date)
                  ->get();
  }


  /**
   * Initializes the model.
   */
  public static function boot()
  {
      parent::boot();

      static::deleting(function ($parking) {
          File::delete(public_path().'/aaa/otitos/'.$parking->plaza_photo);
          File::delete(public_path().'/aaa/otitos/'.$parking->plano_photo);
          File::delete(public_path().'/aaa/otitos/'.$parking->street_photo);
      });
  }
}
