<?php

namespace App;

use Carbon\Carbon;
use App\Utils\Interval;

class Booking extends Elegant
{

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'share_id',
      'start_date',
      'end_date'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
      'id'
  ];

  /**
   * The attributes that should be casted as dates.
   *
   * @var array
   */
  protected $dates = [
    'start_date', 'end_date',
  ];

  /**
   * The attributes that should be hidden for plain arrays.
   *
   * @var array
   */
  protected $forbidden = ['user_id'];

  /**
   * Determines if the model should be timestamped.
   *
   * @var boolean
   */
  public $timestamps = false;


  /**
   * Set start_date attribute.
   *
   * @param Carbon $date
   */
  public function setStartDateAttribute($date)
  {
      $this->attributes['start_date'] = $date->second(0);
  }

  /**
   * Set start_date attribute.
   *
   * @param Carbon $date
   */
  public function setEndDateAttribute($date)
  {
      $this->attributes['end_date'] = $date->second(0);
  }

  /**
   * Get share which belongs to this booking.
   */
  public function share()
  {
      return $this->belongsTo('App\Share');
  }

  /**
   * Get the user which has made the booking.
   */
  public function user()
  {
      return $this->belongsTo('App\User');
  }

  /**
   * Get the parking of the booking which has made the booking.
   *
   * @return App\Parking
   */
  public function parking()
  {
      return $this->share->parking;
  }

  /**
   * Scope a query to only include active Bookings.
   *
   * @return \Illuminate\Database\Eloquent\Builder
   */
  public function scopeActive($query)
  {
      return $query->where('end_date', '>=', Carbon::now())
                   ->orderBy('start_date')
                   ->get();
  }

  /**
   * Return the Booking as an Interval model
   *
   * @return App\Interval
   */
  public function getIntervalAttribute(){
    return new Interval($this->start_date,$this->end_date);
  }

  /**
   * Get description attribute.
   *
   * @return String
   */
  public function getDescriptionAttribute()
  {
      return $this->interval->description();
  }

  /**
   * Scope to return Intervals.
   *
   * @return Collection<Interval>
   */
  public function scopeIntervals($query)
  {
      $bookings = $query->orderBy('start_date')->get();
      if ($bookings->count() == 0) {
          return null;
      }
      return $bookings->map(function($booking){
        return $booking->interval;
      });
  }

  /**
   * Scope to return Intervals.
   *
   * @return Collection<Interval>
   */
  public function scopeToday($query)
  {
      $today = Carbon::now()->startOfDay();;
      return $query->where('start_date','>=',$today)->orderBy('start_date','asc');
  }

}
