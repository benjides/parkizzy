<?php

namespace App;

use App\Elegant;

class Car extends Elegant
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'plate_num',
      'make',
      'model',
      'colour',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
      'id', 'user_id'
  ];

  /**
   * Determines if the model should be timestamped.
   *
   * @var boolean
   */
  public $timestamps = false;

  /**
   * Get the user that owns the car.
   */
  public function user()
  {
    return $this->belongsTo('App\User');
  }

}
