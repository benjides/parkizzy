<?php

namespace App;

use App\Elegant;

class Code extends Elegant
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'trama_tiempos',
      'trama_desbordamientos',
      'bytes_c',
      'validation_c',
      'bytes_tiempos',
      'bytes_desbordamientos',
      'keyfob_mac'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
      'id', 'parking_id'
  ];


  /**
   * Determines if the model should be timestamped.
   *
   * @var boolean
   */
  public $timestamps = false;



  /**
   * Get the user that owns the car.
   */
  public function parking()
  {
    return $this->belongsTo('App\Parking');
  }

}

