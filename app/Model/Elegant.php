<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Elegant extends Model
{

  /**
   * The attributes that should be hidden for plain arrays.
   *
   * @var array
   */
  protected $forbidden = ['user_id' , 'created_at' , 'updated_at'] ;

  /**
   * Returns a simple array of the attributes
   *
   * @return array
   */
  public function getArray()
  {
    $attributes = [];
    foreach ($this->attributes as $key => $attribute) {
      if (!in_array($key,$this->forbidden)) {
        $attributes[] = $this->$key;
      }
    }
    return $attributes;
  }

}
