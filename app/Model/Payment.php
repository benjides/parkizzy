<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Payment extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'user_id',
      'booking_id',
      'start_date',
      'end_date',
      'active'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
      'id'
  ];


  /**
   * The attributes that should be casted into dates.
   *
   * @var array
   */
   protected $dates = ['start_date','end_date'];


  /**
   * Determines if the model should be timestamped.
   *
   * @var boolean
   */
  public $timestamps = false;



  /**
   * Get the user that owns the keyfob.
   */
  public function user()
  {
    return $this->belongsTo('App\User');
  }


  /**
   * Get the booking of the payment.
   */
  public function booking()
  {
    return $this->belongsTo('App\Booking');
  }


  public function checkOut()
  {
  	$this->active = false;
  	$this->end_date = Carbon::now();
  	$this->save();
  }


  public function scopeActive($query)
  {
  	return $query->where('active',1);
  }

  /**
   * Initializes the model.
   *
   * @return void
   */
  public static function boot(){
  	parent::boot();

    // Once created start counting time is spent
  	static::creating(function ($payment) {
  		$payment->active = true;
  		$payment->start_date = Carbon::now();
  	});
  }

}
