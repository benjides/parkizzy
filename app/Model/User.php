<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon;
use App\Mailers\UserMailer;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'firstName',
        'lastName',
        'phone',
        'date',
        'parking',
        'car',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'token', 'id',
    ];

     /**
      * The attributes that should be casted into dates.
      *
      * @var array
      */
     protected $dates = ['date'];

     /**
      * Determines if the model should be timestamped.
      *
      * @var bool
      */
     public $timestamps = false;

    /**
     * Initializes the model.
     */
    public static function boot()
    {
        parent::boot();

        static::creating(function ($user) {
            $user->token = str_random(30);
        });

        static::created(function ($user) {
            UserMailer::sendEmailConfirmation($user);
        });
    }

    /**
     * Set the password attribute.
     *
     * @param string $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    /**
     * Set the birthday attribute.
     *
     * @param string $date
     */
    public function setDateAttribute($date)
    {
        $this->attributes['date'] = Carbon\Carbon::createFromFormat('!d-m-Y', $date);
    }

    /**
     * Confirm the user.
     */
    public function confirmEmail()
    {
        $this->verified = true;
        $this->token = null;
        $this->save();
    }

    /**
     * Proceeds to delete the user.
     */
    public function proceedDelete()
    {
        $this->token = str_random(30);
        $this->save();
        UserMailer::sendDeleteConfirmation($this);
    }

    /**
     * Books a Share
     *
     * @param  App\Share $share           Share where the booking is done
     * @param  App\Interval $interval     From when to when.
     *
     */
    public function book($share,$interval) {
      // Check if
      // The User is free at the interval
      // The Book is no off limits in its subintervals
      $booking = new Booking([
        'share_id' => $share->id,
        'start_date' => $interval->start_date,
        'end_date' => $interval->end_date
      ]);
      $this->bookings()->save($booking);
      return $booking;
    }

    /**
     * Get the cars owned by a user.
     */
    public function cars()
    {
        return $this->hasMany('App\Car');
    }

    /**
     * Get the parkings owned by a user.
     */
    public function parkings()
    {
        return $this->hasMany('App\Parking');
    }

    /**
     * Get the home owned by a user.
     */
    public function home()
    {
        return $this->hasOne('App\Home');
    }

    /**
     * Get all of the shares a User has posted for the country.
     */
    public function shares()
    {
        return $this->hasManyThrough('App\Share', 'App\Parking');
    }

    /**
     * Get all of the bookings the User has made.
     */
    public function bookings()
    {
        return $this->hasMany('App\Booking');
    }

    /**
     * Get all of the bookings the User has made.
     */
    public function billing()
    {
        return $this->hasOne('App\Billing');
    }

    /**
     * Get the keyfob of the user.
     */
    public function keyfob()
    {
        return $this->hasOne('App\Keyfob');
    }

    /**
     * Get the payments of the user.
     */
    public function payments()
    {
        return $this->hasMany('App\Payment');
    }


}
