<?php

namespace App;

use App\Elegant;

class Keyfob extends Elegant
{
  
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'keyfob_mac',
      'keyfobnum_serie',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
      'id', 'user_id'
  ];


  /**
   * Determines if the model should be timestamped.
   *
   * @var boolean
   */
  public $timestamps = false;



  /**
   * Get the user that owns the keyfob.
   */
  public function user()
  {
    return $this->belongsTo('App\User');
  }
}
