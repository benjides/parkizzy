<?php

namespace App\Utils;

use Carbon\Carbon;
use Illuminate\Support\Collection;

class Interval
{
    /**
     * States the moment it is available.
     *
     * @var Carbon/Carbon
     */
    public $start_date;

    /**
     * States the moment when is no longer available.
     *
     * @var Carbon/Carbon
     */
    public $end_date;

    public function __construct(Carbon $start_date = null, Carbon $end_date = null, $start_time = null, $end_time = null)
    {
        if ($start_time) {
            $time = explode(':', $start_time);
            $start_date->hour($time[0])->minute($time[1])->second(0);
        }
        $this->start_date = $start_date;

        if ($end_time) {
            $time = explode(':', $end_time);
            $end_date->hour($time[0])->minute($time[1])->second(0);
        }
        $this->end_date = $end_date;
    }

    public function __toString()
    {
        return $this->description();
    }

    /**
     * Returns a description of the interval.
     *
     * @return string
     */
    public function description()
    {
        return 'Desde las '.$this->start_date->format('H:i').' del '.$this->start_date->format('d-m-Y')
        .' hasta las '.$this->end_date->format('H:i').' del '.$this->end_date->format('d-m-Y');
    }

  /**
   * States if a date is in the Interval.
   *
   * @param  Carbon $date
   *
   * @return bool
   */
  public function between(Carbon $date, $equal = true)
  {
      return $date->between($this->start_date, $this->end_date, $equal);
  }

  /**
   * States if a interval is included in the Interval.
   *
   * @param  Interval $interval
   *
   * @return bool
   */
  public function includes(Interval $interval)
  {
      $first = !$interval->start_date ? true : $this->between($interval->start_date, false);
      $second = !$interval->end_date ? true : $this->between($interval->end_date, false);

      return $first && $second;
  }

  /**
   * States if a interval is partially included in the Interval.
   *
   * @param  Interval $interval
   *
   * @return int      [0 => fully included, 1=> partially included , 2 => not included]
   */
  public function includesPartial(Interval $interval)
  {
      $first = $this->between($interval->start_date) ? 0 : 1;
      $second = $this->between($interval->end_date) ? 0 : 1;

      return $first + $second;
  }

  /**
   * Return the minutes the Interval lasts.
   *
   * @return int minutes
   */
  public function size()
  {
      return $this->start_date->diffInMinutes($this->end_date);
  }

  /**
   *  Shrinks the intervals limits by the margin value.
   *
   * @param  Collection $intervals
   * @param  int        $shrink    (minutes)
   *
   * @return Collection $intervals
   */
  public static function shrink(Collection $intervals, $shrink = 10)
  {
    for ($i=1; $i < sizeof($intervals) - 1 ; $i++) {
      $intervals[$i]->start_date->addMinutes($shrink);
      $intervals[$i]->end_date->subMinutes($shrink);
    }
      // $intervals = $intervals->map(function ($interval) use ($shrink) {
      //     $interval->start_date->addMinutes($shrink);
      //     $interval->end_date->subMinutes($shrink);
      //
      //     return $interval;
      // });

      return self::tune($intervals);
  }

  /**
   * Removes Intervals which are not a minimun size.
   *
   * @param Collection $intervals
   * @param int        $margin    (minutes)
   *
   * @return Collection $intervals
   */
  public static function tune(Collection $intervals, $margin = 10)
  {
      return $intervals->reject(function ($interval) use ($margin) {
          return $interval->size() < $margin || $interval->start_date > $interval->end_date  ;
      })->values();
  }
}
