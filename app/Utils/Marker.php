<?php

namespace App\Utils;

class Marker
{
    /**
     * Returns a Marker Object as an Array.
     *
     * @return array
     */
    public static function make($parking, $share, $interval, $status)
    {
        $app = [
          $parking->street_parking.','.$parking->num_street_parking,
          $parking->id,
          $share->id,
        ];
        $marker = [
          'address' => $parking->street_parking,
          'number' => $parking->num_street_parking,
          'lat' => $parking->lat_parking,
          'long' => $parking->long_parking,
          'status' => $status,
          'message' => $interval->description(),
          'app' => implode('||', $app),
        ];

        return $marker;
    }

    /**
     * Returns a non-available Marker Object as an Array.
     *
     * @return array
     */
    public static function unavailable($parking)
    {
        $app = [
          $parking->street_parking.','.$parking->num_street_parking,
          $parking->id,
          0,
        ];
        $marker = [
          'address' => $parking->street_parking,
          'number' => $parking->num_street_parking,
          'lat' => $parking->lat_parking,
          'long' => $parking->long_parking,
          'status' => 2,
          'message' => 'El parking no esta disponible para compartir',
          'app' => implode('||', $app),
        ];

        return $marker;
    }


}
