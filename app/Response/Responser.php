<?php
namespace App\Response;

class Responser
{

  public static function sucess($code,$status = array(), $message = "OK"){
    return response(self::response($code,true,$status,$message), 200)
          ->header('Content-Type', 'text/plain');
  }

  public static function error($code,$status = array(), $message = "OK"){
    return response(self::response($code,true,$status,$message), 400)
          ->header('Content-Type', 'text/plain');
  }

  public static function response($code,$result,$status,$message){
    $data['url_code'] = $code;
    $data['result'] = $result ? 'ok' : 'error';
    if ($status) {
      if (is_array($status[0])) {
        for ($i=0; $i < count($status) ; $i++) {
          $status[$i] = implode('}{',$status[$i]);
        }
        $data['status'] = implode(')(' , $status);
      }else{
        $data['status'] = implode('}{',$status);
      }
    }else {
      $data['status'] = null;
    }
    $data['message'] = $message;
    return implode('][',$data);
  }

}
