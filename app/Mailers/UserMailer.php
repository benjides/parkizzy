<?php
namespace App\Mailers;

use App\User;
use Mail;

class UserMailer
{
    /**
     * Deliver the email confirmation.
     *
     * @param  User $user
     * @return void
     */
    public static function sendEmailConfirmation(User $user)
    {
      Mail::send('emails.confirm', ['user' => $user], function ($message) use ($user) {
          $message->from(config('mail.from.address'));
          $message->to($user->email);
          $message->subject('Activación de cuenta');
      });
    }


    /**
     * Deliver the email deletion.
     *
     * @param  User $user
     * @return void
     */
    public static function sendDeleteConfirmation(User $user)
    {
      Mail::send('emails.delete', ['user' => $user], function ($message) use ($user) {
          $message->from(config('mail.from.address'));
          $message->to($user->email);
          $message->subject('Eliminación de cuenta');
      });
    }

}
