<?php

namespace App\Http\Controllers;

use App\Parking;
use App\Utils\Interval;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use App\User;

class WebController extends Controller
{
    public function __construct()
    {
        $this->middleware('webBeta');
    }

    public function index()
    {
        $markers = Parking::available(new Interval(Carbon::now(), Carbon::now()));

        return view('web.index')->with(['markers' => $markers]);
    }

    public function showLogin()
    {
        if (Auth::user()) {
            return redirect('/');
        }

        return view('web.login');
    }
    public function login(Request $request)
    {
        $user = User::where('email', $request->input('mail'))->first();
        if (!$user) {
            return redirect()->back()->withInput()->with('message', 'El usuario no existe');
        }
        $user = [
          'email' => $request->input('mail'),
          'password' => $request->input('password'),
          'verified' => true,
        ];
        $remeber = $request->has('remeber') ? true : false;
        if (Auth::attempt($user, $remeber)) {
            return redirect()->intended('/');
        }

        return redirect()->back()->withInput()->with('message', 'Los credenciales no concuerdan');
    }
    public function showRegister()
    {
        if (Auth::user()) {
            return redirect('/');
        }

        return view('web.register');
    }
    public function register(Request $request)
    {
        $this->validate($request, [
            'email' => 'bail|required|unique:users|email',
            'firstName' => 'required',
            'lastName' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
            'phone' => 'digits:9'
        ]);
        $data = $request->all();
        $data['latitude'] = 0;
        $data['longitude'] = 0;
        $data['car'] = false;
        $data['parking'] = false;
        $data['date'] = $data['day'].'-'.$data['month'].'-'.$data['year'];
        $user = User::create($data);
        if ($user) {
            return view('web.register-sucess');
        }
        return redirect()->back();
    }

    public function search(Request $request)
    {
        $markers = Parking::available(new Interval(Carbon::now(), Carbon::now()));

        return view('web.search')->with(['markers' => $markers]);
    }

    public function howto()
    {
        return view('web.howto');
    }
}
