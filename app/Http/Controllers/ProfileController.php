<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('webBeta');
        $this->middleware('auth');
    }

    public function profile() {
      return view('web.profile');
    }

    public function bookings() {
      return view('web.bookings');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }


}
