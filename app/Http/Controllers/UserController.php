<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

class UserController extends Controller
{
  public function confirmEmail($token)
  {
      User::whereToken($token)->firstOrFail()->confirmEmail();
      return view('user.confirm');
  }
  public function confirmDelete($token)
  {
      $user = User::whereToken($token)->firstOrFail();
      $user->delete();
      return view('user.delete');
  }
}
