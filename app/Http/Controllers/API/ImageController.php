<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Parking;

class ImageController extends Controller
{

    public function apiupload(Request $request)
    {
      $parking = Parking::findorFail($request->input('parking_id'));
      $filename = $parking->id.'-'.$request->input('use_code').'-'.$request->input('filename');
      if ($request->hasFile('filename')) {
        $file = $request->file('filename');
        $file->move(public_path().'/aaa/otitos/', $filename);
      }
      else{
        $file = file_get_contents('php://input');
        if (!$file) {
          return "Se prodoju un error durante la subida";
        }
        file_put_contents('aaa/otitos/'.$filename, $file);
      }

      switch ($request->input('use_code')) {
        case 10:
          $parking->plaza_photo = $filename;
          break;
        case 20:
          $parking->plano_photo = $filename;
          break;
        case 30:
          $parking->street_photo = $filename;
          break;
      }

      $parking->save();
      return "OK";
    }
}
