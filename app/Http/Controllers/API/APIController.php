<?php

namespace App\Http\Controllers;

use Auth;
use Validator;

use Illuminate\Http\Request;

use App\Response\Responser;
use App\Utils\Interval;
use Carbon\Carbon;

use App\User;
use App\Car;
use App\Parking;
use App\Home;
use App\Billing;
use App\Share;
use App\Booking;
use App\Code;
use App\Keyfob;
use App\Payment;

use Illuminate\Foundation\Auth\ResetsPasswords;


class APIController extends Controller
{
  use ResetsPasswords;

  /**
   *  HTTP request instance.
   *
   * @var Request
   */
  private $request;

  /**
   * Url code.
   *
   * @var string
   */
  private $url_code;

  protected $subject = 'Resetear Contraseña';

  /**
   * Instantiates a new APIController.
   */
  public function __construct()
  {
      $this->middleware('apimiddle');
  }

  /**
   * Call the proper method according to the url_code.
   *
   * @param Request $request
   *
   * @return Response
   */
  public function index(Request $request)
  {
      $this->request = $request;
      $url_code = $request->input('url_code');
      $this->url_code = $url_code;
      $route = camel_case($url_code);
      return $this->$route();
  }

  /**
   * Attemps to login the User with the provided credentials.
   *
   * @return Response
   */
  private function login()
  {
      $user = [
      'email' => $this->request->input('user_email'),
      'password' => $this->request->input('user_password'),
      'verified' => true,
    ];
      if (Auth::attempt($user)) {
          return Responser::sucess($this->url_code, [],'Login correcto');
      }
      return Responser::error($this->url_code, [], 'Usuario o contraseña incorrecta');
  }

  /**
   * User Routes.
   */

  /**
   * Registers a new user into the database.
   *
   * @return Response
   */
  private function userSave()
  {
      $input = $this->request->all();
      $data = [
      'email' => $input['user_email'],
      'password' => $input['user_password'],
      'firstName' => $input['first_name'],
      'lastName' => $input['last_name'],
      'phone' => $input['mobile_phone'],
      'date' => $input['birth_date'],
      'parking' => isset($input['active_parking']),
      'car' => isset($input['active_car']),
      'longitude' => $input['register_long'],
      'latitude' => $input['register_lat'],
    ];
      $user = null;
      $message = null;
      if (!$this->request->user()) {
          $rules = [
        'email' => 'required|email|unique:users',
        'password' => 'required',
        'firstName' => 'required',
        'lastName' => 'required',
        'phone' => 'required|digits:9',
        'date' => 'required',
      ];
          $validator = Validator::make($data, $rules);
          if ($validator->fails()) {
              return Responser::error($this->url_code, null, $validator->errors()->first());
          }
          $user = User::create($data);
          $message = 'Usuario creado correctamente';
      } else {
          $user = $this->request->user();
          $user->fill($data);
          $user->save();
          $message = 'Los campos han sido actualizados';
      }

      return Responser::sucess($this->url_code, [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], $message);
  }

    private function userEdit()
    {
        if (!$this->request->user()) {
            return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
        }
        $user = $this->request->user();
        $response = [$user->email, '', $user->firstName, $user->lastName, $user->phone, $user->date->format('d-m-Y'), $user->parking, $user->car,$user->email];

        return Responser::sucess($this->url_code, $response, 'Sus datos han sido actualizados');
    }

  /**
   * Deletes a user stored in the database.
   *
   * @return Response
   */
  private function userDelete()
  {
      $user = User::whereEmail($this->request->input('user_email'))->first();
      $user->proceedDelete();
      return Responser::sucess($this->url_code, null, 'Le hemos enviado un mail para confirmar su baja y resolver su credito');
  }

  /**
   * Send a reset password email to the user.
   *
   * @return Response
   */
  private function sendPassword()
  {
    $user;
    if (!$this->request->has('user_email')) {
      return Responser::error($this->url_code, null, 'No se ha proporcionado el email del usuario');
    }
    $this->request->request->add(['email' => $this->request->input('user_email')]);
    $this->postEmail($this->request);
    return Responser::sucess($this->url_code, null, 'Se le ha enviado un mail con instrucciones para resetear su contraseña');

  }

  /**
   * Cars Routes.
   */

  /**
   * Registers a new car into the database.
   *
   * @return Response
   */
  private function carSave()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $input = $this->request->all();
      if (!$this->request->input('car_id')) {
          $car = new Car($input);
          $this->request->user()->cars()->save($car);

          return Responser::sucess($this->url_code, [$car->id], 'Coche registrado satisfactoriamente.');
      } else {
          $car = Car::findOrFail($this->request->input('car_id'));
          $car->fill($input);
          $car->save();

          return Responser::sucess($this->url_code, [$car->id], 'Coche editado satisfactoriamente.');
      }
  }

  /**
   * Edits a car stored in the database.
   *
   * @return Response
   */
  private function carEdit()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $cars = $this->request->user()->cars()->get();
      if (!$cars->count()) {
          return Responser::sucess($this->url_code, null, 'El usuario no tiene coches registrados');
      }
      $data = [];
      foreach ($cars as $car) {
          $data[] = $car->getArray();
      }

      return Responser::sucess($this->url_code, $data, count($cars).' registros.');
  }

  /**
   * Deletes a car stored in the database.
   *
   * @return Response
   */
  private function carDelete()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $car = Car::findOrFail($this->request->input('car_id'));
      $car->delete();

      return Responser::sucess($this->url_code, [], 'Coche eliminado satisfactoriamente');
  }

  /**
   * Parking Routes.
   */

  /**
   * Registers a new car into the database.
   *
   * @return Response
   */
  private function parkingSave()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $input = $this->request->all();
      if (!$this->request->input('parking_id')) {
          $parking = new Parking($input);
          $this->request->user()->parkings()->save($parking);

          return Responser::sucess($this->url_code, [$parking->id], 'Parking registrado satisfactoriamente.');
      } else {
          $parking = Parking::findOrFail($this->request->input('parking_id'));
          $parking->fill($input);
          $parking->save();

          return Responser::sucess($this->url_code, [$parking->id], 'Parking editado satisfactoriamente.');
      }
  }

  /**
   * Edits a parking stored in the database.
   *
   * @return Response
   */
  private function parkingEdit()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $parkings = $this->request->user()->parkings()->get();
      if (!$parkings->count()) {
          return Responser::sucess($this->url_code, null, 'El usuario no tiene parkings registrados');
      }
      $data = [];
      foreach ($parkings as $parking) {
          $data[] = $parking->getArray();
      }

      return Responser::sucess($this->url_code, $data, count($parkings).' registros.');
  }

  /**
   * Deletes a parking stored in the database.
   *
   * @return Response
   */
  private function parkingDelete()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $parking = Parking::findOrFail($this->request->input('parking_id'));
      $parking->delete();

      return Responser::sucess($this->url_code, [], 'Parking eliminado satisfactoriamente');
  }

  /**
   * Home Routes.
   */

  /**
   * Registers a new home into the database.
   *
   * @return Response
   */
  private function homeAddressSave()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $input = $this->request->all();
      if (!$this->request->input('home_id')) {
          $home = new Home($input);
          $this->request->user()->home()->save($home);

          return Responser::sucess($this->url_code, [$home->id], 'Casa registrada satisfactoriamente.');
      } else {
          $home = Home::findOrFail($this->request->input('home_id'));
          $home->fill($input);
          $home->save();

          return Responser::sucess($this->url_code, [$home->id], 'Casa editada satisfactoriamente.');
      }
  }

  /**
   * Edits a home stored in the database.
   *
   * @return Response
   */
  private function homeAddressEdit()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $home = $this->request->user()->home;
      if (!$home) {
          return Responser::sucess($this->url_code, null, 'El usuario no tiene casa registrada');
      }
      return Responser::sucess($this->url_code, $home->getArray(), '1 registro.');
  }

  /**
   * Deletes a home stored in the database.
   *
   * @return Response
   */
  private function homeAddressDelete()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $home = Home::findOrFail($this->request->input('home_id'));
      $home->delete();

      return Responser::sucess($this->url_code, [], 'Casa eliminada satisfactoriamente');
  }

  /**
   * Billing Routes.
   */

  /**
   * Registers a new billing address into the database.
   *
   * @return Response
   */
  private function billingAddressSave()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $input = $this->request->all();
      if (!$this->request->input('billing_address_id')) {
          $billing = new Billing($input);
          $this->request->user()->billing()->save($billing);

          return Responser::sucess($this->url_code, [$billing->id], 'Direccion registrada satisfactoriamente.');
      } else {
          $billing = Billing::findOrFail($this->request->input('billing_address_id'));
          $billing->fill($input);
          $billing->save();

          return Responser::sucess($this->url_code, [$billing->id], 'Direccion editada satisfactoriamente.');
      }
  }

  /**
   * Edits a billing stored in the database.
   *
   * @return Response
   */
  private function billingAddressEdit()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $billings = $this->request->user()->billing()->get();
      if (!$billings->count()) {
          return Responser::sucess($this->url_code, null, 'El usuario no tiene direcciones registradas');
      }
      $data = [];
      foreach ($billings as $billing) {
          $data[] = $billing->getArray();
      }

      return Responser::sucess($this->url_code, $data, count($billings).' registros.');
  }

  /**
   * Deletes a billing stored in the database.
   *
   * @return Response
   */
  private function billingAddressDelete()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $billing = Billing::findOrFail($this->request->input('billing_address_id'));
      $billing->delete();

      return Responser::sucess($this->url_code, [], 'Direccion eliminada satisfactoriamente');
  }

  /**
   * ParkingShare Routes.
   */

  /**
   * Registers a new parkingShare into the database.
   *
   * @return Response
   */
  private function parkingShareSave()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $rules = [
        'parking_id' => 'required',
        'time_f' => 'required',
        'time_t' => 'required',
        'date_f' => 'required',
        'date_t' => 'required',
        'partial' => 'required'
      ];

      $input = $this->request->all();

      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
          return Responser::error($this->url_code, null, 'No se han proporcionado todos los datos necesarios');
      }
      $share;
      if (!$this->request->input('parking_share_id')) {
          $share = new Share();
      } else {
          $share = Share::findOrFail($this->request->input('parking_share_id'));
      }

      $share->parking_id = $input['parking_id'];
      $share->start_date = Carbon::createFromFormat('!d-m-Y H:i', $input['date_f'].' '.$input['time_f']);
      $share->end_date = Carbon::createFromFormat('!d-m-Y H:i', $input['date_t'].' '.$input['time_t']);
      $share->repeat = $input['partial'];
      $share->save();

      return Responser::sucess($this->url_code, [$share->parking_id, $share->id], 'Share registrado');
  }

  /**
   * Edits a parking share stored in the database.
   *
   * @return Response
   */
  private function parkingShareEdit()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $shares = Parking::where('id', $this->request->input('parking_id'))->first()->shares()->active();

      if (!$shares->count()) {
          return Responser::sucess($this->url_code, null, 'El usuario no tiene shares registradas');
      }
      $data = [];
      foreach ($shares as $share) {
        $data[] = [
         $share->parking->id,
         $share->id,
         $share->description,
       ];
      }

      return Responser::sucess($this->url_code, $data, count($shares).' registros.');
  }

  /**
   * Deletes a parking Share stored in the database.
   *
   * @return Response
   */
  private function parkingShareDelete()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $share = Share::findOrFail($this->request->input('parking_share_id'));
      if ($share->delete()) {
        return Responser::sucess($this->url_code, [], 'Share eliminada satisfactoriamente');
      }
      return Responser::error($this->url_code, [], 'No es posible elimar Shares que tengas Bookings agregados');
  }

  /**
   * ParkingBookings Routes.
   */

  /**
   * Registers a new booking into the database.
   *
   * @return Response
   */
  private function parkingBookSave()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $input = $this->request->all();
      $share = Share::findOrFail($input['parking_share_id']);
      $start_date = Carbon::createFromFormat('!d-m-Y H:i',$input['book_date_f'].' '.$input['book_time_f']);
      $end_date = Carbon::createFromFormat('!d-m-Y H:i',$input['book_date_t'].' '.$input['book_time_t']);
      $interval = new Interval($start_date,$end_date);
      $booking = $this->request->user()->book($share,$interval);
      if (!$booking) {
        return Responser::error($this->url_code, [], 'El parking no esta disponible para este momento');
      }
      return Responser::sucess($this->url_code, [$booking->id], 'Reserva registrada satisfactoriamente');
  }

  /**
   * Edits a booking share stored in the database.
   *
   * @return Response
   */
  private function parkingBookEdit()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $bookings = $this->request->user()->bookings()->active();
      if (!$bookings->count()) {
          return Responser::sucess($this->url_code, null, 'El usuario no tiene bookings');
      }
      $data = [];
      foreach ($bookings as $booking) {
          $parking = $booking->parking();
          $share = $booking->share;
          $response = [
            $booking->id,
            $share->id,
            $parking->id,
            $share->description,
            $parking->address_parking,
            $parking->street_parking,
            $parking->num_street_parking,
            $parking->city_parking,
            $parking->num_parking,
            $parking->level_parking,
            $parking->car_size,
            $parking->user->firstName,
            0,
            $booking->description,
            0,
            0,
            $booking->start_date->format('H:i'),
            $booking->end_date->format('H:i'),
            $booking->start_date->format('d-m-Y'),
            $booking->end_date->format('d-m-Y'),
            $parking->wc,
            $parking->description,
            $parking->plaza_photo,
            $parking->plano_photo,
            $parking->street_photo,
          ];
          $data[] = $response;
      }
      return Responser::sucess($this->url_code, $data, $bookings->count().' registros.');
  }

  /**
   * Deletes a booking stored in the database.
   *
   * @return Response
   */
  private function parkingBookDelete()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $booking = Booking::findOrFail($this->request->input('parking_book_id'));
      if ($booking->delete()) {
          return Responser::sucess($this->url_code, [], 'Booking eliminado satisfactoriamente');
      }
      return Responser::error($this->url_code, [], 'No se ha podido eliminar el share debido a que tiene reservas');
  }

    private function parkingSearch()
    {
        if (!$this->request->user()) {
            return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
        }

        $start_time = $this->request->input('search_time_f');
        $end_time = $this->request->input('search_time_t');
        $start_date = Carbon::createFromFormat('d-m-Y', $this->request->input('search_date_f'));
        $end_date = Carbon::createFromFormat('d-m-Y', $this->request->input('search_date_t'));
        $interval = new Interval($start_date, $end_date, $start_time, $end_time);
        $parking = Parking::findOrFail($this->request->input('parking_id'));
        $avaibility = $parking->avaibility($interval);
        if (!$avaibility) {
            return Responser::error($this->url_code, null, 'No hay disponibilidad para el horario escogido');
        }
        $share = $avaibility['share'];

        $response = [
          $parking->id,
          $share->id,
          $share->description,
          $parking->address_parking,
          $parking->street_parking,
          $parking->num_street_parking,
          $parking->num_parking,
          $parking->level_parking,
          $parking->car_size,
          $parking->user->id,
          0,
          $parking->city_parking,
          $parking->wc,
          $share->start_date->format('H:i'),
          $share->end_date->format('H:i'),
          $share->start_date->format('d-m-Y'),
          $share->end_date->format('d-m-Y'),
          0,
          0,
          $parking->description,
          0,
          0,
          $parking->plaza_photo,
          $parking->plano_photo,
          $parking->street_photo,
        ];
        return Responser::sucess($this->url_code, $response, 'Parking data');
    }

    private function parkingBusyEdit()
    {
        if (!$this->request->user()) {
            return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
        }
        $shares = Parking::where('id', $this->request->input('parking_id'))->first()->shares()->expired();
        if (!$shares->count()) {
            return Responser::sucess($this->url_code, null, 'El usuario no tiene shares registradas');
        }
        $data = [];
        foreach ($shares as $share) {
          $parking = $share->parking;
          $data[] = [
            1,
            $parking->id,
            $share->id,
            0,
            $parking->street_parking,
            $parking->num_street_parking,
            $parking->city_parking,
            $parking->num_parking,
            0,
            'red',
          ];
        }

        return Responser::sucess($this->url_code, $data, count($shares).' registros.');
    }

  /**
   * Code Routes.
   */

  /**
   * Registers a new code associated to a Parking.
   *
   * @return Response
   */
  private function codeSave()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $input = $this->request->all();
      $code = null;
      if ($input['parking_id'] != 0) {
          $code = new Code($input);
          $parking = Parking::findOrFail($input['parking_id']);
          $parking->code()->save($code);
      } else {
          $code = Parking::findOrFail($input['parking_id'])->code()->first();
          $code->fill($input);
          $code->save();
      }

      return Responser::sucess($this->url_code, [$code->id], 'Code registrado satisfactoriamente.');
  }

  /**
   * Edits a billing stored in the database.
   *
   * @return Response
   */
  private function codeEdit()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $code = Parking::findOrFail($this->request->input('parking_id'))->code()->first();
      if (!$code->count()) {
          return Responser::sucess($this->url_code, null, 'El parking no tiene codigo asociado');
      }

      return Responser::sucess($this->url_code, [$code->trama_tiempos, $code->trama_desbordamientos]);
  }

  /**
   * Deletes a code stored in the database.
   *
   * @return Response
   */
  private function codeDelete()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $code = Parking::findOrFail($this->request->input('parking_id'))->code()->first();
      $code->delete();

      return Responser::sucess($this->url_code, [], 'Code eliminado satisfactoriamente');
  }

  /**
   * Deletes a code stored in the database.
   *
   * @return Response
   */
  private function codeVerification()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      return Responser::sucess($this->url_code, [], 'Datos verificados');
  }

  /**
   * keyFob Routes.
   */

  /**
   * Registers a new keyfob associated to a user.
   *
   * @return Response
   */
  private function keyfobSave()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $keyfob = null;
      $input = $this->request->all();
      if ($this->request->user()->keyfob()->count() < 1) {
          $keyfob = new Keyfob($input);
          $this->request->user()->keyfob()->save($keyfob);
      } else {
          $keyfob = $this->request->user()->keyfob;
          $keyfob->fill($input);
          $keyfob->save();
      }

      return Responser::sucess($this->url_code, [$keyfob->user_id], 'Keyfob registrado satisfactoriamente.');
  }

  /**
   * Shows the keyfob of a User stored in the database.
   *
   * @return Response
   */
  private function keyfobEdit()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $keyfob = $this->request->user()->keyfob;
      if (!$keyfob) {
          return Responser::sucess($this->url_code, null, 'El usuario no tiene keyfob asociado');
      }

      return Responser::sucess($this->url_code, [$keyfob->keyfob_mac, '1.0']);
  }

  /**
   * Deletes a keyfob stored in the database.
   *
   * @return Response
   */
  private function keyfobDelete()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $keyfob = $this->request->user()->keyfob;
      $keyfob->delete();

      return Responser::sucess($this->url_code, [], 'keyfob eliminado satisfactoriamente');
  }

    private function carCheckIn()
    {
        if (!$this->request->user()) {
            return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
        }
        $bookings = $this->request->user()->bookings()->today()->get();
        $now = Carbon::now();
        if (!$bookings->count()) {
            return Responser::error($this->url_code, null, 'El usuario no dispone de reservas en este momento');
        }
        $booking = $bookings->first();
        if ($booking->start_date->subMinutes(5) > $now) {
            return Responser::error($this->url_code, null, 'La reserva aun no esta disponible');
        }
        //Everything OK
        $share = $booking->share;
        $parking = $share->parking;
        $code = $parking->code;
        $keyfob = $this->request->user()->keyfob;

        $response = [
          $booking->id,
          $share->id,
          $parking->id,
          $share->description,
          $parking->address_parking,
          $parking->street_parking,
          $parking->num_street_parking,
          $parking->city_parking,
          $parking->num_parking,
          $parking->level_parking,
          $parking->car_size,
          $parking->user->firstName,
          0,
          $booking->description,
          0,
          0,
          $booking->start_date->format('H:i'),
          $booking->end_date->format('H:i'),
          $booking->start_date->format('d-m-Y'),
          $booking->end_date->format('d-m-Y'),
          $parking->wc,
          $parking->description,
          $parking->plaza_photo,
          $parking->plano_photo,
          $parking->street_photo,
          $parking->user->firstName,
          $code->trama_tiempos,
          $code->trama_desbordamientos,
          $keyfob->keyfob_mac,
        ];
        return Responser::sucess($this->url_code, $response, 'Datos');
    }

    private function carCheckOut()
    {
        if (!$this->request->user()) {
            return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
        }
        $payment = $this->request->user()->payments()->active()->first();
        $payment->checkout();

        return Responser::sucess($this->url_code, null, 'Ha consumido '.$payment->start_date->diffInHours($payment->end_date).' horas');
    }

  /**
   * Checks if the user has a booking to access the parking.
   *
   * @return Response
   */
  private function carIn()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }

      //MAKE VALIDATION OF CAR IN & DATABSE OF PAYMENT AND DONE

      //OK send message
      $payment = new Payment();
      $payment->booking_id = $this->request->input('parking_book_id');
      $this->request->user()->payments()->save($payment);

      return Responser::sucess($this->url_code, null, 'Su tiempo empieza a contar desde '.$payment->start_date);
  }

  /**
   * Pedestrian Routes.
   */

  /**
   * Provides the data for the user to get out the parking.
   *
   * @return Response
   */
  private function pedestrian()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }
      $payment = $this->request->user()->payments()->active()->first();
      if (!$payment) {
          return Responser::error($this->url_code, null, 'El usuario no dispone en este momento de ninguna reserva');
      }
      $booking = $payment->booking;
      $share = $booking->share;
      $parking = $share->parking;
      $code = $parking->code;
      $keyfob = $this->request->user()->keyfob;

      $response = [
        $booking->id,
        $share->id,
        $parking->id,
        $share->description,
        $parking->address_parking,
        $parking->street_parking,
        $parking->num_street_parking,
        $parking->city_parking,
        $parking->level_parking,
        $parking->car_size,
        $parking->user->firstName,
        0,
        $booking->description,
        0,
        0,
        $booking->start_date->format('H:i'),
        $booking->end_date->format('H:i'),
        $booking->start_date->format('d-m-Y'),
        $booking->end_date->format('d-m-Y'),
        $parking->city_parking,
        $parking->wc,
        $parking->description,
        $parking->plaza_photo,
        $parking->plano_photo,
        $parking->street_photo,
        $parking->user->firstName,
        $code->trama_tiempos,
        $code->trama_desbordamientos,
        $keyfob->keyfob_mac,
      ];

      return Responser::sucess($this->url_code, $response, 'Datos');
  }

 /**
  * Provides the data for the user to get out the parking.
  *
  * @return Response
  */
 private function pedestrianOut()
 {
     if (!$this->request->user()) {
         return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
     }

     return Responser::sucess($this->url_code, [], 'Datos');
 }

  /**
   * Provides the data for the user to get out the parking.
   *
   * @return Response
   */
  private function pedestrianIn()
  {
      if (!$this->request->user()) {
          return Responser::error($this->url_code, null, 'Debe estar logueado para continuar');
      }

      return Responser::sucess($this->url_code, [], 'Datos');
  }

  /**
   * Checks if the user is the radius of a target.
   *
   * @return bool
   */
  private function isInParking($person, $target, $margin = 1)
  {
      $lat1 = $person[0];
      $lng1 = $person[1];
      $lat2 = $target[0];
      $lng2 = $target[1];
      $pi = 3.14159;
      $rad = doubleval($pi / 180.0);

      $lon1 = doubleval($lng1) * $rad;
      $lat1 = doubleval($lat1) * $rad;
      $lon2 = doubleval($lng2) * $rad;
      $lat2 = doubleval($lat2) * $rad;
      $theta = $lng2 - $lng1;

      $dist = acos(sin($lat1) * sin($lat2) + cos($lat1) * cos($lat2) * cos($theta));

      if ($dist < 0) {
          $dist += $pi;
      }

      $km = doubleval($dist * 115.1666667);

      return $km < $margin;
  }
}
