<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Parking;
use Carbon\Carbon;
use App\Utils\Interval;
use App\Share;

class MapController extends Controller
{
    protected $interval;

    public function __construct(Request $request)
    {
        $now = Carbon::now();

        $start_date;
        $end_date;

        if ($request->has('search_date_f')) {
            $start_date = Carbon::createFromFormat('d-m-Y', $request->input('search_date_f'));
            if ($request->has('search_time_f')) {
                $time = explode(':', $request->input('search_time_f'));
                $start_date->hour($time[0])->minute($time[1])->second(0);
            }
        } else {
            $start_date = $now->copy();
        }
        if ($request->has('search_date_t')) {
            $end_date = Carbon::createFromFormat('d-m-Y', $request->input('search_date_t'));
            if ($request->has('search_time_t')) {
                $time = explode(':', $request->input('search_time_t'));
                $end_date->hour($time[0])->minute($time[1])->second(0);
            }
        } else {
            $end_date = $now->copy();
        }

        $this->interval = new Interval($start_date, $end_date);
    }

    public function mapSearch(Request $request)
    {
        $center = [];
        if ($request->has('parking_id')) {
            $parking = Parking::whereId($request->input('parking_id'))->first();
            $center['lat'] = $parking->lat_parking;
            $center['lng'] = $parking->long_parking;
        }
        $parkings = Parking::all();
        $markers = array();
        foreach ($parkings as $parking) {
            $markers[] = $parking->parkingAvaibility($this->interval);
        }
        $markers = collect($markers);

        return view('api.map')->with(['parkings' => $markers, 'center' => collect($center)]);
    }

    public function mapFull(Request $request)
    {
        $center = [];
        if ($request->has('geo_lat') && $request->has('geo_long')) {
            $center['lat'] = (float) $request->input('geo_lat');
            $center['lng'] = (float) $request->input('geo_long');
        }
        $markers = Parking::available($this->interval);

        return view('api.map')->with(['parkings' => $markers, 'center' => collect($center)]);
    }

    public function test()
    {
      $parking = Parking::whereId(147)->first();
      return $parking->avaibility($this->interval);
    }

}
