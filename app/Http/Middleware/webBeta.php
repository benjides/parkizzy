<?php

namespace App\Http\Middleware;

use Closure;

class webBeta
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (env('WEB_BETA',true) == true) {
            if (($request->input('user') == 'root' && $request->input('password') == 'root')
                || $request->session()->has('permission')) {
                $request->session()->put('permission', 'true');

                return $next($request);
            } else {
                return redirect('unavailable');
            }
        }

        return $next($request);
    }
}
