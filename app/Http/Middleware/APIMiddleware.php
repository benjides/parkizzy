<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class APIMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->has('url_code'))
            return redirect('/');
        return $next($request);

    }
}
