<?php


/**
 * WEB ROUTES
 */
Route::get('/', 'WebController@index');
Route::any('/search', 'WebController@search');
Route::get('/howto', 'WebController@howto');


Route::get('/login', 'WebController@showLogin');
Route::post('/login', 'WebController@login');
Route::get('/register', 'WebController@showRegister');
Route::post('/register', 'WebController@register');

/**
 * AUTH ROUTES
 */
Route::get('/logout', 'ProfileController@logout');
Route::get('/profile', 'ProfileController@profile');
Route::get('/bookings', 'ProfileController@bookings');

Route::get('/unavailable', function(){
  return view('unavailable');
});

/**
 * API ROUTES
 */
Route::group(['prefix' => '/zand/{lang}/amtroip'], function () {
    Route::post('/runapp', 'APIController@index');

    Route::get('/mapfull', 'MapController@mapFull');

    Route::get('/mapavailable', 'MapController@mapSearch');

    Route::match(['get', 'post'], '/imgsave', 'ImageController@apiupload');
});

Route::get('register/confirm/{token}', 'UserController@confirmEmail');
Route::get('delete/confirm/{token}', 'UserController@confirmDelete');

Route::get('/password', 'PasswordReset@getEmail');
Route::post('/password', 'PasswordReset@postEmail');
Route::get('/password/reset/{token}', 'PasswordReset@getReset');
Route::post('/password/reset', 'PasswordReset@postReset');
