<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;

class Interceptor extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register(Request $request)
    {
        if ($request->has('url-code')) {
          $route = '/'.$request->get('url-code');
          Request::instance()->server->set('REQUEST_URI',$route);
        }

    }
}
