<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // Generate Users,Parkings and Shares
        $users = factory(App\User::class, 100)->create();
        foreach ($users as $user) {
            $random = rand(2, 5);
            $parkings = factory(App\Parking::class, $random)->make();
            foreach ($parkings as $parking) {
                $user->parkings()->save($parking);
                $count = rand(2, 35);
                $shares = factory(App\Share::class, $count)->make();
                foreach ($shares as $share) {
                    $parking->shares()->save($share);
                }
            }
        }

        // Generate Bookings
        $shares = App\Share::all();
        foreach ($shares as $share) {
            $end_date = $share->start_date;
            for ($i = 0; $i < rand(0, 5); ++$i) {
                $user = $users->random();
                $start_date = $end_date->copy()->addDays(rand(1, 5))->addHours(rand(0, 2))->addMinutes(rand(0, 50));
                $end_date = $start_date->copy()->addHours(rand(0, 2))->addMinutes(rand(0, 50));
                $interval = new App\Utils\Interval($start_date, $end_date);
                $user->book($share, $interval);
            }
        }
    }
}
