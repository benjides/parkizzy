<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('address')->nullable();
            $table->string('street');
            $table->integer('num_street')->unsigned()->nullable();
            $table->integer('num_block')->unsigned();
            $table->integer('floor')->unsigned();
            $table->integer('door')->unsigned();
            $table->integer('zip')->unsigned();
            $table->string('city');
            $table->string('province');

            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('billings');
    }
}
