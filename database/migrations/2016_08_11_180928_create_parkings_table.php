<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParkingsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('parkings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('address_parking')->nullable();
            $table->string('street_parking');
            $table->integer('num_street_parking')->unsigned();
            $table->integer('level_parking');
            $table->integer('num_parking')->unsigned();
            $table->integer('zip_parking')->unsigned();
            $table->string('city_parking');
            $table->string('province_parking');
            $table->integer('car_size')->unsigned();
            $table->boolean('wc');
            $table->boolean('cctv');
            $table->boolean('indoor');
            $table->integer('street_level')->unsigned();
            $table->boolean('easy');
            $table->boolean('doorman');
            $table->text('description_parking');
            $table->string('plaza_photo')->nullable();
            $table->string('plano_photo')->nullable();
            $table->string('street_photo')->nullable();
            $table->decimal('lat_parking', 8, 5);
            $table->decimal('long_parking', 8, 5);
            $table->boolean('rolling');
            $table->boolean('verify_opener')->nullable();
            $table->integer('code_1')->unsigned()->nullable();
            $table->integer('code_2')->unsigned()->nullable();
            $table->integer('code_3')->unsigned()->nullable();

            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('parkings');
    }
}
