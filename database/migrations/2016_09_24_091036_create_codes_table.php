<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('codes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parking_id')->unsigned();
            $table->text('trama_tiempos');
            $table->text('trama_desbordamientos');
            $table->integer('bytes_c');
            $table->integer('validation_c')->nullable();
            $table->integer('bytes_tiempos');
            $table->integer('bytes_desbordamientos');
            $table->text('keyfob_mac');


            $table->foreign('parking_id')
                  ->references('id')->on('parkings')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');     
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('codes');
    }
}
