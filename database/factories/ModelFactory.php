<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
      'email' => $faker->unique()->email,
      'password' => $faker->password,
      'firstName' => $faker->firstName,
      'lastName' => $faker->lastName,
      'phone' => $faker->randomNumber(9),
      'date' => $faker->date($format = 'd-m-Y'),
      'parking' => $faker->boolean,
      'car' => $faker->boolean,
    ];
});

$factory->define(App\Parking::class, function (Faker\Generator $faker) {
    return [
      'street_parking' => $faker->streetName,
      'num_street_parking' => $faker->randomDigit,
      'level_parking' => $faker->randomDigit,
      'zip_parking' => $faker->postcode,
      'city_parking' => $faker->city,
      'car_size' => $faker->randomDigit,
      'wc' => $faker->boolean,
      'cctv' => $faker->boolean,
      'indoor' => $faker->boolean,
      'street_level' => $faker->randomDigit,
      'easy' => $faker->boolean,
      'doorman' => $faker->boolean,
      'description_parking' => $faker->text,
      'long_parking' => $faker->longitude(-8, 3),
      'lat_parking' => $faker->latitude(37, 43),
      'rolling' => $faker->boolean,
    ];
});

$factory->define(App\Share::class, function (Faker\Generator $faker) {
    $date = Carbon\Carbon::instance($faker->dateTime(Carbon\Carbon::now()->addYear()));
    $add = $faker->randomNumber(2);
    return [
      'start_date' => $date,
      'end_date' => $date->copy()->addDays($add)->addHours($add)->addMinutes($add),
      'repeat' => $faker->regexify('(0|1){7}'),
    ];
});

$factory->define(App\Booking::class, function (Faker\Generator $faker) {
    $date = Carbon\Carbon::instance($faker->dateTime(Carbon\Carbon::now()->addYear()));
    $add = $faker->randomNumber(2);

    return [

      'start_date' => $date,
      'end_date' => $date->copy()->addDays($add)->addHours($add)->addMinutes($add),
      'repeat' => $faker->regexify('(0|1){7}'),
    ];
});
